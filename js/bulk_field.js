(function ($) {
  Drupal.behaviors.selectAll = {
    attach: function (context, settings) {
      $('.type_checkbox_select_all').live('click', function(){
        $('.type_checkbox_select').each(function(){
          if($('.type_checkbox_select_all').is(':checked')){
            $(this).attr('checked', true);
          }else{
            $(this).attr('checked', false);
          }
        });
      });
    }
  };
})(jQuery);
