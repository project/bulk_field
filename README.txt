Bulk field

Purpose of Bulk field :
  1. This module solved the frustrating work of creating fields instances to numerous places.
  2. Easy to manage fields instances to Node/Comment/Taxonomy/User.

How to use it:
  Installation:
    1. Download the recommended version of this module from this link (https://www.drupal.org/project/bulk_field).
    2. Extract and place it inside the modules folder. Location like: sites/all/modules/
    3. Go to website and enable this module from the location www.example.com/admin/modules

  Usage:
    1. After Installation once can see Bulk field instances menu in admin_menu structre section.
    2. Or can directly visit to the link www.example.com/admin/structure/bulk_field.
    3. Firstly, select the section you wnat to work with from right side menu(Node/Comment/Taxonomy/User).
    4. Secondly, select the field from Existing field.
    5. And then check the content type/Taxonomy/user account from table you want the field to be instances for and submit.

Comprises section:
  This module works for the following Entity types of drupal:
    1. Nodes (content)
    2. Comments
    3. Taxonomy terms
    4. Users


Note*: www.example.com -> This site url is used as the example. Please change this with your site url.
